package com.velykyi;

import com.velykyi.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Project {
    private static Logger logger1 = LogManager.getLogger(Project.class);
    private List<Task> taskList;

    public Project() {
        this.taskList = new LinkedList<>();

        this.taskList.add(new Task("task 1"));
        this.taskList.add(new Task("task 2"));
        this.taskList.add(new Task("task 3"));
        this.taskList.add(new Task("task 4"));
        this.taskList.add(new Task("task 5"));
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void showTasks() {
        for (Task task : this.taskList) {
            logger1.info(task.getTaskName() + " " + task.getState() + " by " + task.getDeveloperName());
        }
    }
}
