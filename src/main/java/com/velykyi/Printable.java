package com.velykyi;

@FunctionalInterface
public interface Printable {
    void print();
}
