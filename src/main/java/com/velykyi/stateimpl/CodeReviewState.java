package com.velykyi.stateimpl;

import com.velykyi.State;
import com.velykyi.Task;

public class CodeReviewState implements State {
    @Override
    public void toDo(Task task) {
        task.setDeveloperName(null);
        task.setState(new ToDoState());
        logger1.info("Search new developer.");
    }

    @Override
    public void inProgress(Task task) {
        task.setState(new InProgressState());
        logger1.info("Remake task.");
    }

    @Override
    public void done(Task task) {
        task.setState(new DoneState());
    }

    @Override
    public String toString() {
        return "State: code review";
    }
}
