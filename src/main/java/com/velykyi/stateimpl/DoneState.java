package com.velykyi.stateimpl;

import com.velykyi.State;

public class DoneState implements State {
    @Override
    public String toString() {
        return "State: done";
    }
}
