package com.velykyi.stateimpl;

import com.velykyi.State;
import com.velykyi.Task;

public class InProgressState implements State {
    @Override
    public void toDo(Task task) {
        task.setDeveloperName(null);
        task.setState(new ToDoState());
        logger1.info("Search new developer.");
    }

    @Override
    public void codeReview(Task task) {
        task.setState(new CodeReviewState());
    }

    @Override
    public String toString() {
        return "State: in progress";
    }
}
