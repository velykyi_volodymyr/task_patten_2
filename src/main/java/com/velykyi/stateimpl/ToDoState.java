package com.velykyi.stateimpl;

import com.velykyi.State;
import com.velykyi.Task;

public class ToDoState implements State {
    @Override
    public void inProgress(Task task) {
        task.setState(new InProgressState());
    }

    @Override
    public String toString() {
        return "State: to do";
    }
}
