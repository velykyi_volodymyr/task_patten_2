package com.velykyi;

import java.util.*;

public class App {
    private Scanner scannInt = new Scanner(System.in);
    private Scanner scannStr = new Scanner(System.in);
    private Map<String, String> taskMenu;
    private Map<String, Printable> taskMethod;
    private Task task;

    public static void main(String[] args) {
        App app = new App();
        Project project = new Project();
        app.showMenu(project);
    }

    public App() {
        setTaskMethod();
        setTaskMenu();
    }

    private void setTaskMethod() {
        this.taskMethod = new LinkedHashMap<>();
        this.taskMethod.put("1", this::setToDoState);
        this.taskMethod.put("2", this::setInProgressState);
        this.taskMethod.put("3", this::setCodeReviewState);
        this.taskMethod.put("4", this::setDoneState);
    }

    private void setTaskMenu() {
        this.taskMenu = new LinkedHashMap<>();
        this.taskMenu.put("1", "1 - To Do");
        this.taskMenu.put("2", "2 - Get task");
        this.taskMenu.put("3", "3 - Review code");
        this.taskMenu.put("4", "4 - Done");
        this.taskMenu.put("0", "0 - exit");
    }

    private void setDoneState() {
        this.task.done();
    }

    private void setCodeReviewState() {
        this.task.reviewCode();
    }

    private void setInProgressState() {
        if (this.task.getDeveloperName() == null) {
            System.out.println("Enter your name! ");
            this.task.getTask(scannStr.nextLine());
        } else {
            this.task.getTask();
        }
    }

    private void setToDoState() {
        this.task.toDo();
    }

    private void chooseTask(Project project) {
        int index;
        do {
            project.showTasks();
            System.out.println("Choose task.");
            index = scannInt.nextInt() - 1;
        } while (index == -1);
        this.task = project.getTaskList().get(index);
    }

    private void showMenu(Project project) {
        String menuKey;
        do {
            chooseTask(project);
            System.out.println("\nMENU:");
            for (String str : this.taskMenu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point");
            menuKey = scannStr.nextLine();
            try {
                taskMethod.get(menuKey).print();
            } catch (Exception e){

            }
        } while(!menuKey.equals("0"));
    }
}
