package com.velykyi;

import com.velykyi.stateimpl.ToDoState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task {
    private static Logger logger1 = LogManager.getLogger(Project.class);
    private State state;
    private String developerName;
    private String taskName;

    public State getState() {
        return state;
    }

    public Task(String taskName) {
        this.state = new ToDoState();
        this.taskName = taskName;
    }

    public void toDo() {
        state.toDo(this);
    }

    public void getTask(String developerName) {
        state.inProgress(this);
        this.setDeveloperName(developerName);
    }

    public void getTask() {
        state.inProgress(this);
    }

    public void reviewCode() {
        state.codeReview(this);
    }

    public void done() {
        logger1.info("Task was done by " + this.getDeveloperName());
        state.done(this);
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
