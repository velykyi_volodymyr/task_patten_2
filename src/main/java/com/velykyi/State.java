package com.velykyi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger logger1 = LogManager.getLogger(State.class);
    default void toDo(Task task) {
        logger1.info("toDo - is not allowed!");
    }
    default void inProgress(Task task) {
        logger1.info("inProgress - is now allowed!");
    }
    default void codeReview(Task task) {
        logger1.info("codeReview - is not allowed!");
    }
    default void done(Task task) {
        logger1.info("done - is not allowed!");
    }
}
